# Wjcore Spigot Plugin

Wjcore is a Spigot plugin designed to enhance your Minecraft server with various features such as customized join and leave messages, server announcements, ban management, and link filtering in chat.

## Features

- Customized join and leave messages.
- Periodic server announcements.
- Ban management system.
- Link filtering in chat to prevent sharing of URLs.

## Installation

1. Download the latest release of the Wjcore plugin.
2. Place the downloaded JAR file into the `plugins` directory of your Spigot server.
3. Restart or reload your server.

## Usage

- No additional setup is required after installation. The plugin automatically handles join and leave messages, server announcements, ban management, and link filtering.

## Commands

- There are no commands available for this plugin. All functionality is automated.

## Configuration

- The plugin's configuration file (`config.yml`) stores the list of banned players. You can edit this file manually if needed.

## Contributing

Contributions to the Wjcore plugin are welcome! If you have any suggestions, bug reports, or feature requests, please feel free to open an issue or submit a pull request on the GitHub repository.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

Feel free to customize and expand upon this README to suit your needs!