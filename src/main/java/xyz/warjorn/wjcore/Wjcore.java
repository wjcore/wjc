package xyz.warjorn.wjcore;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Wjcore extends JavaPlugin implements Listener {

    private List<String> banList = new ArrayList<>();

    @Override
    public void onEnable() {
        // Load banned players from the configuration file
        this.saveDefaultConfig();
        banList = this.getConfig().getStringList("banlist");

        getServer().getPluginManager().registerEvents(this, this);
        getServer().getScheduler().runTaskTimer(this, this::announce, 0, 6000); // 5 minutes (6000 ticks)
    }

    @Override
    public void onDisable() {
        // Save banned players to the configuration file
        this.getConfig().set("banlist", banList);
        this.saveConfig();
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (isBanned(player)) {
            player.kickPlayer(ChatColor.RED + "You are banned from this server.");
            return;
        }
        event.setJoinMessage(ChatColor.GREEN + "[✔️] " + player.getName());
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        event.setQuitMessage(ChatColor.RED + "[❌] " + player.getName());
    }

    @EventHandler
    public void onServerPing(ServerListPingEvent event) {
        event.setMotd(ChatColor.GRAY + "Welcome to the server!");
    }

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        String message = event.getMessage();
        if (isBanned(player)) {
            event.setCancelled(true);
            return;
        }
        if (containsLink(message)) {
            // You can handle links by replacing them with a placeholder or cancelling the message
            event.setCancelled(true);
            player.sendMessage(ChatColor.RED + "Links are not allowed in chat.");
            return;
        }
        event.setMessage(ChatColor.GRAY + message); // Set chat message color to gray
    }

    public void announce() {
        Bukkit.broadcastMessage(ChatColor.GRAY + "Created by WARJORN Chicago");
    }

    public boolean isBanned(Player player) {
        return banList.contains(player.getUniqueId().toString());
    }

    public void banPlayer(Player player) {
        if (!isBanned(player)) {
            banList.add(player.getUniqueId().toString());
            player.kickPlayer(ChatColor.RED + "You are banned from this server.");
        }
    }

    public void unbanPlayer(Player player) {
        if (isBanned(player)) {
            banList.remove(player.getUniqueId().toString());
        }
    }

    public boolean containsLink(String message) {
        Pattern pattern = Pattern.compile("https?://\\S+|www\\.\\S+");
        Matcher matcher = pattern.matcher(message);
        return matcher.find();
    }
}
